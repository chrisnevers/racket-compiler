#include <stdio.h>

void print_int(int i)
{
    printf("%d\n", i);
}

int read_int()
{
    int i;
    scanf("%d", &i);
    return i;
}
